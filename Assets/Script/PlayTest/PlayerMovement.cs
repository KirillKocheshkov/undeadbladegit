﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using NSubstitute;

namespace Tests
{
    
    public class PlayerMovement
    {
        [UnityTest]

        public IEnumerator press_forward_move_worvard()
        {
            GameObject plaerGO = new GameObject("Player");
            PlayerTEst movementComponent = plaerGO.AddComponent<PlayerTEst>();
            movementComponent.PlayerInput = Substitute.For<IPlayerInput>();
            movementComponent.PlayerInput.Vertical.Returns(1f); 

            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(plaerGO.transform);
            cube.transform.position = Vector2.zero;

            
            yield return new WaitForSeconds(1f);
            Assert.IsTrue(plaerGO.transform.position.z > 0);
        }
       
    }
}
