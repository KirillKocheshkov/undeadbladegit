﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class PlayerTEst : MonoBehaviour
{
    private Rigidbody _rigidbody;
   private float movespeed = 10f;
   public IPlayerInput PlayerInput {get;set;}

    // Start is called before the first frame update

    private void Awake() 
{
    _rigidbody = GetComponent<Rigidbody>();
    _rigidbody.useGravity = false;
}
    // Update is called once per frame
    void Update()
    {
       float vertical = PlayerInput.Vertical;
       _rigidbody.AddForce(0,0,vertical*movespeed);
    }
}

internal class PlayerInput : IPlayerInput
{
    public float Vertical => Input.GetAxis("Vertical");
}
public interface IPlayerInput
{
    float Vertical{get;}
}