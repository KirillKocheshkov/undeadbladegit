﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementControllerMain : MonoBehaviour, IControllableMovement
{
    // Start is called before the first frame update
    [SerializeField]  Jump currentJumpHandler;
    
    
    protected HorizontalMovementMain currentHorizontalMovement;
       

    public abstract void Jump();
    

    public abstract void MoveDown();
    

    public abstract void MoveLeft();
    

    public abstract void MoveRight();
   

    public abstract void MoveUP();
   
}

[System.Serializable]
       class Jump
        {
            public bool HasVariableJumpHeight = true;
            public int AirJumpAllowed = 1;
            public float MaxHeight = 3.5f;
            public float MinHeight = 1.0f;
            public float TimeToApex = .4f;
            public float OnLadderJumpForce = 0.4f;

            public float FallingJumpPaddingTime = 0.09f;
            public float WillJumpPaddingTime = 0.15f;
        }
    /// сделать отдельный скрипт, передавать через фабрику ;
        
        