﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class HorizontalHumanoid : HorizontalMovementMain
{
    Rigidbody2D rigid;
    private void Awake()
    {
        rigid =GetComponent<Rigidbody2D>();
    }
    private void Start()
    {
        
        gravity = -(2*jumpHight)/Mathf.Pow(timeToReachApex,2);
        jumpVelocity = Mathf.Abs(gravity * timeToReachApex);
    }
    
    

    public override void ExternalForce(ref Vector2 currentVelocity)
    {
        throw new System.NotImplementedException();
    }

    public override void MoveLeft()
    {
        
       float alpha = Input.GetAxis("Horizontal");
       alpha = alpha < -AccelerationTimeGrounded? alpha : 0;
       rigid.velocity =  new Vector2 (Speed * alpha,rigid.velocity.y);
       
    }

    public override void MoveRight()
    {
       float alpha = Input.GetAxis("Horizontal");
       alpha = alpha > AccelerationTimeGrounded? alpha : 0;
       rigid.velocity = new Vector2 (Speed * alpha,rigid.velocity.y);
       
      
    }
}
