﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class   HorizontalMovementMain :MonoBehaviour
{
   /// сделать отдельный скрипт, передавать через фабрику ;
          
        
    public float Speed = 8;
    public float MaxSpeed = 20;
    public float jumpHight;
    public float timeToReachApex;
    public float externalForce;
    public float gravity;
    protected float jumpVelocity;
    protected float AccelerationTimeGrounded =0.1f;

    public float JumpVelocity { get => jumpVelocity;  }

    public abstract void MoveRight();
    public abstract void MoveLeft();
    
    public abstract void ExternalForce(ref Vector2 currentVelocity);
    
        
}
