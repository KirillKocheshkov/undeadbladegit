﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TestPlayerInventoryCharacter //: MonoBehaviour
{
   Dictionary<EquipmentType,Equipment> slots = new Dictionary<EquipmentType, Equipment>();
   List<Equipment> unequipedItems = new List<Equipment>() ;
   private readonly ICharacter _character;

    public ICharacter Character => _character;

    public TestPlayerInventoryCharacter(ICharacter character)
{
    _character = character;
}
   public void Equipmentitem(Equipment item)
   {
       if(slots.ContainsKey(item.type))
       {
           unequipedItems.Add(slots[item.type]);
       }
       slots[item.type] = item;
       _character.OnItemEquiped(item);
   }
   public Equipment GetItem(EquipmentType slot)
   {
        if(slots.ContainsKey(slot))
        {
           return slots[slot] ;
        }
        return null;
   }

   public int GetTotalArmor()
   {
       int totalArmor = slots.Values.Sum(t => t.armorAmount);
       return totalArmor;
   }

}
