﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DamageCalculator 
{
   
 public static int  CalculateDamage(int dmg, float medigation)
    {
        float multiplayer = 1f - medigation;
        return Convert.ToInt32(dmg * multiplayer) ;
    }
  public static int  CalculateDamage(int dmg, ICharacter character)
    {
     int totalArmor =character.inventory.GetTotalArmor() + (character.Level * 10);
     float amount = 100f - totalArmor;
        amount/= 100f;
        return Convert.ToInt32(dmg * amount) ;
    }
  
}
