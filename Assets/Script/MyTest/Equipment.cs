﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment 
{
    public EquipmentType type;
    public int armorAmount;
}

public enum EquipmentType
{
    Helmet,
    Body,
    Boots,
    Arms
}