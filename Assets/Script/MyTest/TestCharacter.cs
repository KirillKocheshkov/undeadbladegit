﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TestCharacter : MonoBehaviour, ICharacter
{
    
    int health;
    
    public int Health { get => health; set => health = value; }

    public int Level  {get;set;}
    public TestPlayerInventoryCharacter inventory { get;}

    public void OnItemEquiped(Equipment item)
    {
        Debug.Log($"You equiped an item {item} in {item.type}");
    }
}
public interface ICharacter
{
    TestPlayerInventoryCharacter inventory {get;}
    int Health{get; }
    int Level {get; }

    void OnItemEquiped(Equipment item);
}

