﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Healpers 
{
    
    public static void GameObjectHelper<T> ( this GameObject gameObject, Action<T> handler)
    {
        var component = gameObject.GetComponent<T>();
        if(component != null)
        {
            handler?.Invoke(component);
        }
    }
}
