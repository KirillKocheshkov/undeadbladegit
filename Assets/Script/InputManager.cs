﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class InputManager 
{
    IControllableMovement currentController;
    
    public InputManager(PlayerController pc)
    {
        Observable.EveryUpdate().Where(_ =>Input.GetAxis("Horizontal")>0 ).Subscribe(_ => pc.MoveRight());
        Observable.EveryUpdate().Where(_ =>Input.GetAxis("Horizontal")<0 ).Subscribe(_ => pc.MoveLeft());
        Observable.EveryUpdate().Where(_ =>Input.GetButtonDown("Jump") ).Subscribe(_ => pc.Jump());
        Observable.EveryUpdate().Where(_ =>Input.GetButtonDown("Use") ).Subscribe(_ => pc.Interact());
    }

   
    
}
