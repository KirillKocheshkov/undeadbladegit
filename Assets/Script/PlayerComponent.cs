﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class PlayerComponent : MonoBehaviour
{
   private PlayerController playerController;
   private PlayerMovementComponent playerMovementComponent;
   
    public static PlayerComponent Instance {get;private set;}
    public PlayerController PlayerController { get => playerController;  }
    public PlayerMovementComponent PlayerMovementComponent { get => playerMovementComponent;  }

    private void Awake() 
    {
      playerController = GetComponent<PlayerController>();
      playerMovementComponent = GetComponent<PlayerMovementComponent>();
      Instance = this;
    }
}
