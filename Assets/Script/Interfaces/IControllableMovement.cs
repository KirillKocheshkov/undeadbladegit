﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IControllableMovement 
{
    void MoveRight();
    void MoveLeft();
    void MoveUP();
    void MoveDown();
    void Jump();
    
}
