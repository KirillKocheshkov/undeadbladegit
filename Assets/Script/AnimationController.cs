﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class AnimationController : MonoBehaviour
{
    // возможно в дальнейшем рефакторить этот компонент, чтоб можно было юзать на других сущностях. больше абстракций богу абстракций.
    PlayerMovementComponent _movenentComponent;
    Animator _animatorController;
   [SerializeField] Transform targetToRotate;
    bool isFacingRight;


    private void Awake() 
    {
        _movenentComponent = GetComponent<PlayerMovementComponent>();
        _animatorController = GetComponent<Animator>();
    }

    private void Start()
    {
      // setting rotation and movement animation
      _movenentComponent.Movement
      .Subscribe( PlayerMovement => {
        _animatorController.SetFloat("HorizontalSpeed",Mathf.Abs(PlayerMovement.x) );
        if(PlayerMovement.x>0.1f) isFacingRight =true;
        if(PlayerMovement.x < -0.1f) isFacingRight = false;
         targetToRotate.localScale = isFacingRight== true? new Vector3 (1,1,1):new Vector3 (-1,1,1);
        })
        .AddTo(this);
        // Set Falling state
        _movenentComponent.isFalling.Subscribe(isFallling => {_animatorController.SetBool("IsFalling", isFallling);}).AddTo(this);
    }
}
