﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class PossessableUnitBase : MonoBehaviour
{
  public MovementControllerMain MovementComp {get;protected set;}
  public PossesCmponentMain PossesionGiver {get;protected set; }
  event Action OnPossess;
  
 public abstract void  Possess (PossessableUnitBase target);
    
}
