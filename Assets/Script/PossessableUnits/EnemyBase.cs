﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : PossessableUnitBase
{
    public override void Possess(PossessableUnitBase target)
    {
        
    }
    private void Awake()
    {
        MovementComp = GetComponent<MovementControllerMain>(); 
        PossesionGiver = GetComponentInChildren<PossesCmponentMain>();
    }
}
