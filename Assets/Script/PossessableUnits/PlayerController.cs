﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[RequireComponent(typeof (PlayerMovementComponent))]
public class PlayerController : PossessableUnitBase, IControllableMovement
{
#region  (Deligates)
    private Action _rightMovement;
    private Action _leftMovement;
    private Action _upMovement;
    private Action _downMovement;
    private Action _jumpMovement;
    private Action _Interaction;
    private InputManager input;
    
    #endregion
    #region  (Components)
    
   [SerializeField] Transform mainCamera;
   
    public event Action OnPossess ;
    #endregion

    

    private void Awake() 
    {
       
       MovementComp = GetComponent<MovementControllerMain>(); 
       PossesionGiver = GetComponentInChildren<PossesCmponentMain>();

    }
private void Start()
{
    input = new InputManager(this);
    Possess(this);
   
}

    
    public void Interact()
    {
        
        if(PossesionGiver.TargetUnit != null)
        {
            Debug.Log( "Used");
            Possess(PossesionGiver.TargetUnit);
            
        }
    }

    public void Jump()
    {
        if(_jumpMovement != null)
        {
            _jumpMovement();
        }
    }

    public void MoveDown()
    {
        if(_downMovement != null)
        {
            _downMovement();
        }
    }

    public void MoveLeft()
    {
        if(_leftMovement != null)
        {
            _leftMovement();
        }
        
    }

    public void MoveRight()
    {
       if(_rightMovement!= null)
       {
           _rightMovement();
       }
       
       
    }

    public void MoveUP()
    {
        if(_upMovement != null)
        {
            _upMovement();
        }
    }
    #region  (Possesion)
 public override void Possess(PossessableUnitBase target)
    {
        PossessMovement(target);
        HandlePossesion(target);

    }

    private void HandlePossesion(PossessableUnitBase target)
    {
        
        gameObject.tag = "Possessable";
        PossesionGiver = target.PossesionGiver;
        target.gameObject.tag = "Player";
        mainCamera.SetParent(target.transform);
    }

    private void PossessMovement(PossessableUnitBase target)
    {
        _rightMovement = target.MovementComp.MoveRight;
        _leftMovement = target.MovementComp.MoveLeft;
        _upMovement = target.MovementComp.MoveUP;
        _downMovement = target.MovementComp.MoveDown;
        _jumpMovement = target.MovementComp.Jump;
    }
   #endregion
}
    

