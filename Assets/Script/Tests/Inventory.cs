﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;



public class Inventory 
{
   [Test]
   public void only_allows_item_per_slot()
   {
    TestPlayerInventoryCharacter inventory = new TestPlayerInventoryCharacter(null);
    Equipment item = new Equipment(){type = EquipmentType.Body, armorAmount = 5};
    Equipment item2 = new Equipment(){type = EquipmentType.Body, armorAmount = 2};
    inventory.Equipmentitem(item);
    inventory.Equipmentitem(item2);

    Equipment equipedItem = inventory.GetItem(EquipmentType.Body);
    Assert.AreEqual(equipedItem, item2);
   }
}
