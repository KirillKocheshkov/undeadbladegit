﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class damage_calculator
    {
        // A Test behaves as an ordinary method
        [Test]
        public void sets_damage_to_half_with_50_medigation()
        {
            int dmg = 10;
            float medigation = 0.5f;
            int expected = 5;
            int finaldamage = DamageCalculator.CalculateDamage(dmg, medigation);

            Assert.AreEqual(expected,finaldamage);
        }
         [Test]
            public void sets_damage_to_2_with_80_medigation()
        {
            int dmg = 10;
            float medigation = 0.8f;
            int expected = 2;
            int finaldamage = DamageCalculator.CalculateDamage(dmg, medigation);

            Assert.AreEqual(expected,finaldamage);
        }
        
        
    }
}
