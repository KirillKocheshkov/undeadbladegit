using NUnit.Framework;
using NSubstitute;

namespace Undead_Blade.Assets.Script.Tests
{
    public class character_with_inventory_test
    {
        [Test]
        public void with_19Armor_take_10_percent_damage()
        {
            ICharacter character = Substitute.For<ICharacter>();
            TestPlayerInventoryCharacter inventory = new TestPlayerInventoryCharacter(character);
            Equipment pants = new Equipment(){type = EquipmentType.Boots, armorAmount = 40};
            Equipment shiled = new Equipment(){type = EquipmentType.Body, armorAmount = 50};
            inventory.Equipmentitem(pants);
            inventory.Equipmentitem(shiled);
            character.inventory.Returns(inventory);

           int calculatedDamage =  DamageCalculator.CalculateDamage(1000,character);

           Assert.AreEqual(100, calculatedDamage);

        }
        [Test]
        public void tells_player_what_he_has_equiped()
        {
            ICharacter character = Substitute.For<ICharacter>();
            TestPlayerInventoryCharacter inventory = new TestPlayerInventoryCharacter(character);
            Equipment pants = new Equipment(){type = EquipmentType.Boots, armorAmount = 40};
            
            inventory.Equipmentitem(pants);
            

           

           character.Received().OnItemEquiped(pants);

        }
    }

    
        
    
}