﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DefaultTriggerPossesor : PossesCmponentMain
{
    public override PossessableUnitBase TargetUnit { get ; protected set ; }
    // Mb make database for this tags;
    string possesTag = "Possessable";
    
    Collider2D currentCollider;

   

    private void OnTriggerEnter2D(Collider2D other) 
    {
        /// make it via object pool
        if(other.CompareTag(possesTag))
        {
            Debug.Log( gameObject.name + " Got" + other.gameObject.name);
            TargetUnit = other.gameObject.GetComponent<PossessableUnitBase>();
            currentCollider = other;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag(possesTag) && currentCollider == other)
        {
            TargetUnit = null;
          
        }
    }
}

